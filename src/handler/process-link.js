"use strict";

const AWS = require("aws-sdk");
const QUEUE = process.env.QUEUE_URL;
const urlRegex = new RegExp(
  "^(?!mailto:)(?:(?:http|https|ftp)://)(?:\\S+(?::\\S*)?@)?(?:(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[0-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,})))|localhost)(?::\\d{2,5})?(?:(/|\\?|#)[^\\s]*)?$",
  "i"
);

const handler = event => {
  const { link } = JSON.parse(event.body);

  if (!urlRegex.test(link))
    return _response(400, { error: "Link is required" });

  const sqs = new AWS.SQS();

  return sqs
    .sendMessage({
      MessageBody: link,
      QueueUrl: QUEUE
    })
    .promise()
    .then(() => _response(202))
    .catch(err => _response(err.statusCode, err.message));
};

const _response = (status, responseBody) => {
  const response = {
    statusCode: status,
    headers: {
      "Access-Control-Allow-Origin": "*"
    }
  };

  if (responseBody) response.body = JSON.stringify(responseBody);

  return Promise.resolve(response);
};

module.exports = { handler };
