"use strict";

const AWS = require("aws-sdk-mock");
const should = require("chai").should();
const urls = require("../../mocks/urls");
const processLink = require("../../../src/handler/process-link");

describe("Process Link Handler", () => {
  beforeEach(() => AWS.restore());

  it("Should throw error for invalid body", () => {
    return processLink.handler({ body: JSON.stringify({}) }).then(res => {
      should.equal(res.statusCode, 400);
    });
  });

  it(`Should return 400 for invalid link`, () => {
    AWS.mock("SQS", "sendMessage", () => Promise.resolve());
    return processLink
      .handler({ body: JSON.stringify({ link: "ww.gng.qwerasfdas" }) })
      .then(res => {
        should.equal(res.statusCode, 400);
      });
  });

  urls.forEach(url => {
    it(`Should return 200 for link ${url}`, () => {
      AWS.mock("SQS", "sendMessage", () => Promise.resolve());
      return processLink
        .handler({ body: JSON.stringify({ link: url }) })
        .then(res => {
          should.equal(res.statusCode, 202);
        });
    });
  });

  it("Should return 400 for error sending sqs message", () => {
    AWS.mock("SQS", "sendMessage", () =>
      Promise.reject({ statusCode: 400, message: "AccessDeniedException" })
    );
    return processLink
      .handler({ body: JSON.stringify({ link: "https://google.com.br" }) })
      .catch(err => {
        should.equal(err.statusCode, 400);
        should.equal(err.message, "AccessDeniedException");
      });
  });
});
